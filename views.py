import abc
import logging

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.db import transaction
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.forms.formsets import formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template import loader
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.dateparse import parse_datetime
from django.contrib.auth.models import User

from django.conf import settings

from website.models import *
from website.forms import *


logger = logging.getLogger(__name__)
CONTENT_PAGINATE_BY = 9
LOGIN_URL = '/account/login/'
MSG_SESSION_EXPIRED = u"La tua sessione è scaduta, \
    ti invitiamo a provare di nuovo."


class Logout(TemplateView):
    def get(self, request, **kwargs):
        logout(request)
        request.session.clear()
        return HttpResponseRedirect(reverse('website:home'))


class Login(TemplateView):
    template_name = 'website/account/login.html'
    logging_error_msg = \
        u"Il nome utente o la password non corrispondono"

    def login_error(self):
        messages.error(self.request, self.logging_error_msg)
        return HttpResponseRedirect(reverse('website:login'))

    def post(self, request):
        """Gather the username and password provided by the user.
        This information is obtained from the login form.
        """
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the credentials are correct.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                user_profile = UserProfile.objects.filter(user__id=user.id)[0]
                if (user_profile.avatar is not None
                    and user_profile.avatar != ''):
                    request.session['user_avatar_img_path'] = (
                        UserProfile.objects
                        .filter(user__id=user.id)[0].avatar.path
                    )
                return HttpResponseRedirect(reverse('website:home'))
            else:
                # An inactive account was used - no logging in!
                messages.error(self.request, self.logging_error_msg)
                return self.login_error()
        else:
            # Bad login details were provided. So we can't log the user in.
            return self.login_error()


def csrf_failure(request, reason=""):
    """
    ctx = {'message': 'some custom messages'}
    return render_to_response(your_custom_template, ctx)
    """
    logger.warning("Error csrf")
    return HttpResponseRedirect(reverse('website:home'))


class CommonChangePassword(FormView):
    template_name = "website/account/password_change.html"
    # TODO: change me
    success_url = '/account/reset_password_done/'
    form_class = ChangePasswordForm

    def change_password(self, form, user):
        # The form validator needs the instance to validate the
        # password
        form.instance = user
        if form.is_valid():
            new_password = form.cleaned_data['new_password2']
            user.set_password(new_password)
            user.save()
            return self.form_valid(form)
        else:
            form.add_error(None, u'La tua password non è stata cambiata.')
            return self.form_invalid(form)


class ChangePassword(LoginRequiredMixin, CommonChangePassword):
    """ Used when the user simply wants to change its password """
    login_url = LOGIN_URL

    def post(self, request, pk, *args, **kwargs):
        if request.user.id != int(pk):
            return HttpResponseRedirect(reverse('website:home'))
        user = User.objects.get(pk=pk)
        form = self.form_class(request.POST)
        if user is not None:
            return self.change_password(form, user)
        else:
            logger.error("User with id '%s' couldn't be found" % pk)
            # TODO redirect to system error page 
            return self.form_invalid(form)


class ResetPasswordRequest(FormView):
    template_name = "website/account/password_reset_request.html"
    success_url = '/account/reset_password_link_sent/'
    form_class = PasswordResetRequestForm

    @staticmethod
    def validate_email_address(email):
        try:
            validate_email(email)
            return True
        except ValidationError:
            return False

    def get(self, request, *args, **kwargs):
        context = super(ResetPasswordRequest, self).get_context_data(**kwargs)
        return render(
            request,
            self.template_name,
            context,
        )

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            email_entered = form.cleaned_data["email"]
        else:
            return self.form_invalid(form)
        if self.validate_email_address(email_entered) is True:
            associated_users= User.objects.filter(email=email_entered)
            if associated_users.exists():
                for user in associated_users:
                    c = {
                        'email': user.email,
                        'domain': settings.PASSWORD_RESET_DOMAIN,
                        'site_name': settings.PASSWORD_RESET_SITE_NAME,
                        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                        'user': user,
                        'token': default_token_generator.make_token(user),
                        'protocol': settings.PASSWORD_RESET_PROTOCOL,
                        }
                    # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
                    subject_template_name='website/account/password_reset_subject.txt'
                    # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
                    email_template_name='website/account/password_reset_email.html'
                    subject = loader.render_to_string(subject_template_name, c)
                    # Email subject *must not* contain newlines
                    subject = ''.join(subject.splitlines())
                    email = loader.render_to_string(email_template_name, c)
                    send_mail(subject, email, settings.DEFAULT_FROM_EMAIL , [user.email], fail_silently=False)
                http_response = self.form_valid(form)
                messages.success(request, u'Una mail è stata inviata a ' + email_entered +". Segui le istruzioni che riceverai per reimpostare la password.")
                return http_response

            form.add_error(None, u'Questo indirizzo mail non è stato registrato.')
            return self.form_invalid(form)

        form.add_error(None, u'Mail non valida.')
        return self.form_invalid(form)


class ResetPasswordConfirm(CommonChangePassword):
    template_name = "website/account/password_reset_change_password.html"
    success_url = '/account/reset_password_done/'
    form_class = ChangePasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            return self.change_password(form, user)
        else:
            form.add_error(None, u'Il link per reimpostare la password non è più valido.')
            return self.form_invalid(form)


class ContentsFilter(ListView):
    context_object_name = 'content_list'
    paginate_by = CONTENT_PAGINATE_BY

    def _get_view_url_start(self):
        raise not NotImplementedError

    def get_queryset(self):
        """ This method is automatically called before get_context_data """
        qs = None
        cc_id = self.kwargs.get('cc_id')
        if cc_id is not None:
            if int(cc_id) > 0:
                qs = Content.objects.list_by_category(cc_id)
            elif int(cc_id) == 0:
                qs = Content.objects.list_no_category()

        content_type = self.kwargs.get('content_type')
        if content_type == Content.TYPE_POST:
            qs = qs.list_posts()\
                if qs is not None else Content.objects.list_posts()
        elif content_type == Content.TYPE_EVENT:
            qs = qs.list_events()\
                if qs is not None else Content.objects.list_events()
        elif content_type == Content.TYPE_TUTORIAL:
            qs = qs.list_tutorials()\
                if qs is not None else Content.objects.list_tutorials()

        return qs.order_by('-user_created_on')\
            if qs is not None\
            else Content.objects.all().order_by('-user_created_on')

    def get_context_data(self, **kwargs):
        # This call will automatically put
        # self.object_list in context['content_list']
        context = super(ContentsFilter, self).get_context_data(**kwargs)
        context['content_category_list'] = ContentCategory.objects.all()
        context['cc_id'] = self.kwargs.get('cc_id')\
            if self.kwargs.get('cc_id') is not None else '-1'
        context['content_type'] = self.kwargs.get('content_type')\
            if self.kwargs.get('content_type') is not None else 'all'
        return context


class Home(ContentsFilter):
    template_name = 'website/index.html'


class ContentsByPerson(ContentsFilter):
    template_name = 'website/person.html'

    def _get_profile_id(self):
        person_id = self.kwargs.get('person_id')
        if person_id is None:
            # We provide user_id instead
            # TODO check if self.kwargs['user_id'] exists
            person_id = UserProfile.objects.get(user__id=self.kwargs['user_id']).id
        return person_id

    def get_queryset(self):
        """ This method is automatically called before get_context_data """
        logger.debug(self.kwargs)
        qs = super(ContentsByPerson, self).get_queryset()
        person_id = self._get_profile_id()
        my_contents = self.kwargs.get('my_contents')
        if my_contents == 'all' or my_contents is None:
            return (
                qs.filter(user_created_by__id=person_id) |
                qs.list_liked_by(person_id)
            )
        elif my_contents == 'personal':
            return qs.filter(user_created_by__id=person_id)
        elif my_contents == 'like':
            return qs.list_liked_by(person_id)
        else:
            logger.error(
                "Invalid parameter for my_contents: '%s'" % my_contents
            )
            return Content.objects.none()

    def get_context_data(self, **kwargs):
        # This call will automatically put
        # self.object_list in context['content_list']
        context = super(ContentsByPerson, self).get_context_data(**kwargs)
        person_id = self._get_profile_id()
        context['person'] = UserProfile.objects.get(
            pk=person_id
        )
        context['my_contents'] = self.kwargs.get('my_contents')\
            if self.kwargs.get('my_contents') is not None else 'all'
        return context


class SearchResults(ListView):
    #context_object_name = 'item_list'
    paginate_by = CONTENT_PAGINATE_BY

    def post(self, request, *args, **kwargs):
        self.object_list = list()
        if request.POST['search'] is not None and request.POST['search'] != '':
            self.object_list = self.get_object_list(
                request.POST['search']  # Defined in the subclasses
            )
        # This call will automatically put
        # self.object_list in context['content_list']

        # Will call the method of the subclass
        context = self.get_context_data(**kwargs)
        context['search'] = request.POST['search']
        context['are_results'] = len(self.object_list) > 0
        SearchResults.template_name = self.get_template_name()
        return self.render_to_response(context)

    @abc.abstractmethod
    def get_object_list(self, search_text):
        raise NotImplementedError

    @abc.abstractmethod
    def get_template_name(self):
        raise NotImplementedError


class SearchResultsContents(SearchResults):
    def get_object_list(self, search_text):
        return Content.objects.search(search_text).order_by('-user_created_on')

    def get_context_data(self, **kwargs):
        context = super(SearchResults, self).get_context_data(**kwargs)
        context['search_by'] = 'contents'
        context['other_search_view_url'] = 'website:search_results_persons'
        return context

    def get_template_name(self):
        return 'website/search_results_contents.html'


class SearchResultsPersons(SearchResults):
    def get_object_list(self, search_text):
        return UserProfile.objects.search(search_text)

    def get_context_data(self, **kwargs):
        context = super(SearchResults, self).get_context_data(**kwargs)
        context['search_by'] = 'persons'
        context['other_search_view_url'] = 'website:search_results_contents'
        return context

    def get_template_name(self):
        return 'website/search_results_persons.html'


class FilterPersons(ListView):
    template_name = 'website/filter_persons.html'
    paginate_by = CONTENT_PAGINATE_BY

    def get_queryset(self):
        logger.debug(self.kwargs)
        if self.kwargs.get('content_id'):
            return UserProfile.objects.like_content(
                content_id=self.kwargs['content_id']
            )
        elif self.kwargs.get('user_category_id'):
            return UserProfile.objects.filter(
                category__id=self.kwargs['user_category_id']
            )
        elif self.kwargs.get('person_id')\
            and self.kwargs.get('filter') == 'followers':
            logger.debug("PERSON ID:%s" % self.kwargs.get('person_id'))
            return (
                UserProfile.objects
                .get(pk=self.kwargs.get('person_id'))
                .list_followers()
            )
        elif self.kwargs.get('person_id')\
            and self.kwargs.get('filter') == 'following':
            return (
                UserProfile.objects
                .get(pk=self.kwargs.get('person_id'))
                .list_following()
            )
        elif self.kwargs.get('person_id')\
            and self.kwargs.get('filter') == 'close_to_me':
            return (
                UserProfile.objects
                .get(pk=self.kwargs.get('person_id'))
                .list_close_to_me()
            )
        else:
            logger.error(
                "Wrong parameter given, here are the args: '%s': "
                % self.kwargs
            )
            return list()


class AccountAddUpdate(TemplateView):
    template_name = 'website/account/account_edit.html'

    def create_get_context(self, step, user=None, user_profile=None, **kwargs):
        """ Is called from the get method in the subclasses"""

        context = super(AccountAddUpdate, self).get_context_data(**kwargs)
        if step == '1':
            context['up_form'] = UserProfileFormStep1(instance=user_profile, prefix='user_profile')
            if user_profile is None:
                context['user_form'] = DjangoUserCreateForm(instance=user, prefix='user')
            else:
                context['user_form'] = DjangoUserUpdateForm(instance=user, prefix='user')
        elif step == '2':
            if user_profile is not None:  # update mode
                initial_data = {'terms_and_conditions':True}
            else:
                initial_data = None
            context['up_form'] = UserProfileFormStep2(
                instance=user_profile, initial=initial_data
            )
            context['user_category_list'] = UserCategory.objects.all()
        else:
            logger.error("Step number invalid: '%s'" % step)

        context['step'] = step
        return context

    def get_forms_and_context(self, request, step, user=None, user_profile=None, **kwargs):
        """ Is called from the post method in the subclasses"""

        context = super(AccountAddUpdate, self).get_context_data(**kwargs)
        context['step'] = step
        if user_profile is not None:  # update mode
            DjangoUserForm = DjangoUserUpdateForm
        else:
            DjangoUserForm = DjangoUserCreateForm
        if step == '1':
            user_form = DjangoUserForm(request.POST, instance=user, prefix='user')
            up_form = UserProfileFormStep1(
                request.POST, instance=user_profile, 
                prefix='user_profile'
            )
            context['user_form'] = user_form
            context['up_form'] = up_form
            return user_form, up_form, context
        if step == '2':
            if request.session.get('user_form') is None:
                messages.error(request, MSG_SESSION_EXPIRED)
                return None, None, None
            user_form = DjangoUserForm(
                request.session['user_form'], instance=user
            )
            up_form2 = UserProfileFormStep2(
                request.POST, request.FILES, instance=user_profile, 
            )
            return user_form, up_form2, context

    def save_post_data(self, request, user_form, up_form2, user_profile=None):
        if request.session.get('up_form') is None:
            messages.error(request, MSG_SESSION_EXPIRED)
            return None
        up_form1 = UserProfileFormStep1(request.session['up_form'])
        with transaction.atomic():
            user = user_form.save(commit=False)
            # In our system the email is also the username
            user.username = user_form.cleaned_data['email']
            user.save()
            
            if user_profile is not None:
                # We are in update mode so we load the original instance
                # to the form so that the framework knows it is an update
                up_form1.instance = user_profile
            user_profile = up_form1.save(commit=False)
            user_profile.user = user
                
            user_profile.category = up_form2.cleaned_data['category']
            user_profile.avatar = up_form2.cleaned_data['avatar']
            user_profile.save()
            return user_profile


class AccountAdd(AccountAddUpdate):  
    def get(self, request, step, **kwargs):
        request.session['create_account_token'] = True
        context = self.create_get_context(step, **kwargs)
        return render(
            request,
            self.template_name,
            context,
        )

    def post(self, request, step, **kwargs):
        if step == '1':
            user_form, up_form, context = self.get_forms_and_context(request, step, **kwargs)
            if user_form is None or up_form is None:
                return HttpResponseRedirect(reverse('website:generic_error_page'))

            if up_form.is_valid() and user_form.is_valid():
                users = User.objects.filter(email=user_form.cleaned_data['email'])
                if users.exists():
                    user_form.add_error('email', u'Questa mail è già registrata.')
                    return render(
                        request,
                        self.template_name,
                        context,
                    )
                request.session['user_form'] = user_form.cleaned_data
                request.session['up_form'] = up_form.cleaned_data
                return HttpResponseRedirect(
                    reverse('website:account_add', args=[2])  # step=2
                )
            else:
                return render(
                    request,
                    self.template_name,
                    context,
                )
                
        elif step == '2':
            if request.session.get('create_account_token') is None:
                # The user probably clickec on the back button and
                # submited the form again => we don't want that
                return HttpResponseRedirect(reverse('website:home'))
            user_form, up_form2, context = self.get_forms_and_context(request, step, **kwargs)
            if user_form is None or up_form2 is None:
                return HttpResponseRedirect(reverse('website:generic_error_page'))

            if up_form2.is_valid() and user_form.is_valid():
                user_profile = self.save_post_data(request, user_form, up_form2)
                del request.session['create_account_token']
                return HttpResponseRedirect(
                    reverse('website:account_created')
                )
            else:
                context['up_form'] = up_form2
                context['user_category_list'] = UserCategory.objects.all()
        return render(
            request,
            self.template_name,
            context,
        )


class AccountUpdate(LoginRequiredMixin, AccountAddUpdate):
    login_url = LOGIN_URL

    def get(self, request, user_id, step, **kwargs):
        if request.user.id != int(user_id):
            return HttpResponseRedirect(reverse('website:home'))
        user = DjangoUser.objects.get(pk=user_id)
        user_profile = UserProfile.objects.get(user__id=user.id)
        context = self.create_get_context(step, user=user, user_profile=user_profile, **kwargs)
        return render(
            request,
            self.template_name,
            context,
        )

    def post(self, request, user_id, step, **kwargs):
        if request.user.id != int(user_id):
            return HttpResponseRedirect(reverse('website:home'))
        user_profile = UserProfile.objects.get(user__id=user_id)
        if step == '1':
            user_form, up_form, context = self.get_forms_and_context(
                request, step, user=user_profile.user, 
                user_profile=user_profile, **kwargs
            )
            if user_form is None or up_form is None:
                return HttpResponseRedirect(reverse('website:generic_error_page'))

            if up_form.is_valid() and user_form.is_valid():
                user = (User.objects
                        .filter(email=user_form.cleaned_data['email'])
                        .exclude(id=user_profile.user.id)
                )
                if user.exists():
                    user_form.add_error('email', u'Questa mail è già registrata.')
                    return render(
                        request,
                        self.template_name,
                        context,
                    )
                request.session['user_form'] = user_form.cleaned_data
                request.session['up_form'] = up_form.cleaned_data
                return HttpResponseRedirect(
                    reverse('website:account_update', args=[user_profile.user.id, 2])  # step=2
                )
            else:
                return render(
                    request,
                    self.template_name,
                    context,
                )
                
        elif step == '2':
            user_form, up_form2, context = self.get_forms_and_context(
                request, step, user=user_profile.user, 
                user_profile=user_profile, **kwargs
            )
            if user_form is None or up_form2 is None:
                return HttpResponseRedirect(reverse('website:generic_error_page'))

            if up_form2.is_valid() and user_form.is_valid():
                user_profile = self.save_post_data(request, user_form, up_form2, user_profile)
                if user_profile is None:
                    return HttpResponseRedirect(reverse('website:generic_error_page'))
                context['title'] = u'Modifica account'
                context['content'] = u'Il tuo account è stato aggiornato correttamente.'
                return render(
                    request,
                    'website/generic_info_page.html',
                    context,
                )
                """
                return HttpResponseRedirect(
                    reverse('website:account_update', args=[user_profile.user.id, 1])  # step=1
                )
                """
            else:
                context['up_form'] = up_form2
                context['user_category_list'] = UserCategory.objects.all()

        return render(
            request,
            self.template_name,
            context,
        )


class ContentDetails(DetailView):
    context_object_name = 'content'

    def get_object(self):
        """ This method is automatically called before get_context_data """
        if self.kwargs['content_type'] == Content.TYPE_POST:
            self.object = Post.objects.get(pk=self.kwargs['pk'])
            ContentDetails.template_name = "website/post_details.html"
        elif self.kwargs['content_type'] == Content.TYPE_EVENT:
            self.object = Event.objects.get(pk=self.kwargs['pk'])
            ContentDetails.template_name = "website/event_details.html"
        elif self.kwargs['content_type'] == Content.TYPE_TUTORIAL\
            or self.kwargs['content_type'] == Content.TYPE_TUTORIAL_PART:
            self.object = Tutorial.objects.get(pk=self.kwargs['pk'])
            ContentDetails.template_name = "website/tutorial_details.html"
        else:
            logger.error("Unknow type: '%s'" % self.kwargs['content_type'])

        return self.object

    """
    def get_context_data(self, **kwargs):
        context = super(ContentDetails, self).get_context_data(**kwargs)
        return context
    """


class ContentAddUpdate(LoginRequiredMixin, TemplateView):
    forms = {
        '1': {
            Content.TYPE_EVENT: EventFormStep1,
            Content.TYPE_POST: PostFormStep1,
            Content.TYPE_TUTORIAL: TutorialFormStep1,
            Content.TYPE_TUTORIAL_PART: TutorialPartFormStep1,
        },
        '2': {
            Content.TYPE_EVENT: EventFormStep2,
            Content.TYPE_POST: PostFormStep2,
            Content.TYPE_TUTORIAL: TutorialFormStep2,
            Content.TYPE_TUTORIAL_PART: TutorialPartFormStep2,
        },
    }

    templates = {
        Content.TYPE_EVENT: 'website/event_edit.html',
        Content.TYPE_POST: 'website/post_edit.html',
        Content.TYPE_TUTORIAL: 'website/tutorial_edit.html',
        Content.TYPE_TUTORIAL_PART: 'website/tutorial_part_edit.html',
    }

    # Create the formset, specifying the form and formset we want to use.
    MediaImageFormSet = formset_factory(MediaImageForm, formset=MediaImageFormSet, extra=2, max_num=5)
    MediaLinksFormSet = formset_factory(MediaLinksForm, formset=MediaLinksFormSet, extra=2, max_num=5)
    media_links_data = [{}]  # Data for the forms
    media_image_data = [{}]

    def create_get_context(self, step, content_type, content=None, **kwargs):
        """ Is called from the get method in the subclasses"""
        context = super(ContentAddUpdate, self).get_context_data(**kwargs)
        context['content_form'] = self.forms[step][content_type](instance=content)
        context['content_type_name'] = Content.TYPES_LABELS[content_type]
        context['step'] = step

        if step == '2':
            context['content_category_list'] = ContentCategory.objects.all()
            context['media_links_formset'] =\
                self.MediaLinksFormSet(initial=self.media_links_data)
            context['media_image_formset'] =\
                self.MediaImageFormSet(initial=self.media_image_data)
        return context

    def post(self, request, step, content_type, content, **kwargs):
        context = super(ContentAddUpdate, self).get_context_data(**kwargs)
        content_form = self.forms[step][content_type](request.POST, instance=content)
        context['content_form'] = content_form
        if step == '1':
            if not content_form.is_valid():
                context['step'] = step
                return render(
                    request,
                    self.templates[content_type],
                    context,
                )
            if content_type == Content.TYPE_EVENT:
                # We have to convert the dates otherwise we get a JSON serialization error
                content_form.cleaned_data['event_on_start'] =\
                    content_form.cleaned_data['event_on_start'].isoformat()
                content_form.cleaned_data['event_on_end'] =\
                    content_form.cleaned_data['event_on_end'].isoformat()
            
            request.session['content_form1'] = content_form.cleaned_data
            
            if content is None:  # Add mode
                return HttpResponseRedirect(
                    reverse('website:content_add', args=[2, content_type])  # step=2
                )
            else:  # Update mode
                return HttpResponseRedirect(
                    reverse('website:content_update', args=[content.id, 2, content_type])
                )
        elif step == '2':
            if content is None and request.session.get('create_content_%s' % content_type) is None:
                # The user probably clicked on the back button and
                # submited the form again => it would duplicate a content
                # that already was submited => we don't want that
                return HttpResponseRedirect(
                    reverse('website:home')
                )
            media_links_formset = self.MediaLinksFormSet(request.POST)
            context['media_links_formset'] = media_links_formset
            media_image_formset = self.MediaImageFormSet(request.POST, request.FILES)
            context['media_image_formset'] = media_image_formset

            if not content_form.is_valid() or not media_links_formset.is_valid()\
                    or not media_image_formset.is_valid():
                context['content_category_list'] = ContentCategory.objects.all()
                context['step'] = step
                return render(
                    request,
                    self.templates[content_type],
                    context,
                )

            content_form1_data = request.session.get('content_form1')
            
            if content_form1_data is None:
                messages.error(request, MSG_SESSION_EXPIRED)
                return HttpResponseRedirect(reverse('website:generic_error_page'))
            
            if content_type == Content.TYPE_EVENT:
                # We previously saved the dates as a string to store them in the session
                # Let's convert them back to a valid django datetime
                content_form1_data['event_on_start'] = parse_datetime(content_form1_data['event_on_start'])
                content_form1_data['event_on_end'] = parse_datetime(content_form1_data['event_on_end'])
            
            form_step1 = self.forms['1'][content_type](content_form1_data)

            # Ugly fix for json serialization error on dates
            del request.session['content_form1']

            if content is not None:  # Update mode
                form_step1.instance = content
            
            new_content = form_step1.save(commit=False)

            if content is None:  # Add mode
                new_content.user_created_by = UserProfile.get_by_user(request.user.id)
            else:  # Update mode
                new_content.user_modified_by = UserProfile.get_by_user(request.user.id)
          
            if content_type == Content.TYPE_TUTORIAL_PART:
                if content is None:
                    main_tutorial = Tutorial.objects.get(id=request.session['main_tutorial_id'])
                    new_content.main_part = main_tutorial
                    del request.session['main_tutorial_id']
                else:
                    new_content.category = content.category
                    new_content.main_part = content.main_part
            else:
                # Update the content with the second form
                new_content.category = content_form.cleaned_data['category']                

            with transaction.atomic():
                new_content.save()
                # Replace the old with the new
                # TODO: fixme for images!
                if content is not None:
                    ContentMedia.objects.filter(content__id=new_content.id, media_type=ContentMedia.VIDEO).delete()
                for media_links_form in media_links_formset:
                    url = media_links_form.cleaned_data.get('url')
                    if url:
                        ContentMedia(url=url, content=new_content).save()
                for media_image_form in media_image_formset:
                    image = media_image_form.cleaned_data.get('image')
                    if image:
                        ContentMedia(image=image, content=new_content).save()
            if content is None:
                del request.session['create_content_%s' % content_type]
            return HttpResponseRedirect(
                reverse('website:content_details', args=[new_content.id, content_type])
            )


class ContentAdd(ContentAddUpdate):
    def get(self, request, step, content_type, **kwargs):
        request.session['create_content_%s' % content_type] = True 
        context = self.create_get_context(step, content_type)
        return render(
            request,
            self.templates[content_type],
            context,
        )

    def post(self, request, step, content_type, **kwargs):
        return super(ContentAdd, self).post(request, step, content_type, content=None, **kwargs)


class ContentUpdate(ContentAddUpdate):
    models = {
        Content.TYPE_EVENT: Event,
        Content.TYPE_POST: Post,
        Content.TYPE_TUTORIAL: Tutorial,
        Content.TYPE_TUTORIAL_PART: Tutorial,
    }

    def get(self, request, content_id, step, content_type, **kwargs):
        content = get_object_or_404(self.models[content_type], pk=content_id)
        if content.user_created_by.user.id != request.user.id:
            # The current user attempts to access a content he is not the author of
            return HttpResponseRedirect(reverse('website:home'))
    
        media_links = ContentMedia.objects.filter(
            content__id=content_id, media_type=ContentMedia.VIDEO
        )
        # Will be used in self.create_get_context
        self.media_links_data = [{'url': l.url} for l in media_links]

        media_images = ContentMedia.objects.filter(
            content__id=content_id, media_type=ContentMedia.IMAGE
        )
        # Will be used in self.create_get_context
        self.media_image_data = [{'image': m.image} for m in media_images]

        context = self.create_get_context(step, content_type, content=content)

        return render(
            request,
            self.templates[content_type],
            context,
        )

    def post(self, request, content_id, step, content_type, **kwargs):
        content = get_object_or_404(self.models[content_type], pk=content_id)
        return super(ContentUpdate, self).post(request, step, content_type, content=content, **kwargs)


@login_required(login_url=LOGIN_URL)
def tutorial_part_add(request, prev_content_id):
    # Get the id of the main part of the tutorial
    prev_tutorial = Tutorial.objects.get(id=prev_content_id)
    if prev_tutorial.main_part is None:
        main_content_id = prev_content_id
    else:
        main_content_id = prev_tutorial.main_part.id
    request.session['main_tutorial_id'] = main_content_id
    return HttpResponseRedirect(
        reverse('website:content_add', args=['1', Content.TYPE_TUTORIAL_PART])
    )

def tutorial_part_display(request, cur_id, direction):
    cur_tutorial = Tutorial.objects.get(id=cur_id)
    if direction == 'next':
        new_tutorial = cur_tutorial.next_part()
    elif direction == 'previous':
        new_tutorial = cur_tutorial.previous_part()
    else:
        logger.error("direction '%s': invalid value" % direction) 
    return HttpResponseRedirect(
        reverse('website:content_details', args=[new_tutorial.id, new_tutorial.content_type])
    )

@login_required(login_url=LOGIN_URL)
def add_comment_to_content(request):
    user_profile = UserProfile.objects.get(user__id=request.user.id)
    content_id = request.POST['content_id']
    content_type = request.POST['content_type']
    comment = request.POST['comment']
    if user_profile:
        content = Content.objects.get(id=content_id)
        if content is None:
            logger.error("Content id not found: %s" % content_id)
        else:
            content.add_comment(user_profile, comment)
    else:
        logger.warning("No user profile found for user: %s" % user)

    return HttpResponseRedirect(
        reverse('website:content_details', args=(content_id, content_type))
    )
