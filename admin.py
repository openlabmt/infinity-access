from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from website.models import *
from sorl.thumbnail.admin import AdminImageMixin

class ContentMediaInline(admin.StackedInline):
    model = ContentMedia
    extra = 3

"""
class TutorialPartMediaInline(admin.StackedInline):
    model = TutorialPartMedia
    extra = 3
"""

class ContentCommentInline(admin.StackedInline):
    model = ContentComment
    extra = 1

class ContentLikeInline(admin.StackedInline):
    model = ContentLike
    extra = 1

"""
class TutorialPartInline(admin.StackedInline):
    model = TutorialPart
    extra = 1
"""

class ContentAdmin(admin.ModelAdmin):
    inlines = [ContentMediaInline, ContentCommentInline, ContentLikeInline]
    exclude = ('content_type', 'content_type_name')

class PostAdmin(ContentAdmin, AdminImageMixin):
    pass

class TutorialAdmin(admin.ModelAdmin):
    pass
    #inlines = [TutorialPartInline]

"""
class TutorialPartAdmin(admin.ModelAdmin):
    inlines = [TutorialPartMediaInline]
"""

# Define an inline admin descriptor for IAUser model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'User profile'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (UserProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(UserCategory)
admin.site.register(ContentCategory)
admin.site.register(Post, PostAdmin)
admin.site.register(Event, ContentAdmin)
admin.site.register(Tutorial, TutorialAdmin)
#admin.site.register(TutorialPart, TutorialPartAdmin)
#admin.site.register(TutorialPartMedia)
