How it works
============

Technical requirements
======================
* Django 1.8.x
* MariaDb / Mysql
  * If you use MariaDb, you have to install another system package to be able to install the python *mysqlclient* module (see below).
  * On Debian Jessie, execute (as root):

```
apt-get install install libmariadb-client-lgpl-dev
ln -s /usr/bin/mysql_config /usr/bin/mariadb_config
```

* Python 3

Installation
------------

* Install required Linux packages
  ```
  apt-get install libjpeg-dev libghc-zlib-dev python3-dev libfreetype6-dev
  ```
  
* Install the required python packages

```
pip install django django-countries
pip install mysqlclient
pip install simplejson
pip install pillow # Images library
pip install pillow # Images library
pip install git+https://github.com/mariocesar/sorl-thumbnail@v12.3
pip install django-embed-video
```

* Create a database using your favorite DBRMS  (eg. mysql, postgresql)

* If you don't already have a django project start one

```
django-admin startproject infinity-access
```

* In *settings.py*: 
 * Adapt the *DATABASES* part according to the DB you created
 * Add: 'website', 'website.templatetags.tag_extras', 'sorl.thumbnail', 'embed_video' to *INSTALLED_APPS*
 * Add email settings:
   <pre>
   EMAIL_USE_TLS = True
   EMAIL_HOST = 'mail_server'
   EMAIL_PORT = port
   EMAIL_HOST_USER = 'user'
   EMAIL_HOST_PASSWORD = 'password'
   DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
   SERVER_EMAIL = EMAIL_HOST_USER
   </pre>
* In the django project directory
 * Open the file urls.py and add:
   <pre>
   url(r'^', include('website.urls', namespace='website')),
   </pre>
* If you wish to install infinity-access website within an existing django project, then... *TODO*

Administration
--------------
1. Initialize database: ./manage.py migrate
2. Run the app : ./manage.py runserver
3. Go to the Django Admin page (e.g. http://127.0.0.1:8000/admin)
4. Set up initial data

Usage
-----
Just go to the start page (e.g. http://127.0.0.1:8000/).

