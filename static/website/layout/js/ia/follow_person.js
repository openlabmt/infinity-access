/* This script is used to follow or not follow a user clicking on a button */

function initFollowButtonsStates() {
    $( "[id^=followButton]" ).each(function( index ) {
        button = this;
        jQuery.ajax({
            async: false,
            type: "POST",
            url: "/ajax/person/is_user_followed/",
            data: "person_id=" + this.value,
            success: function(response) {
                result = JSON.parse(response);
                if (! result) {
                    console.log('error');
                }
                if (result.user_auth) {
                    $(button).show();
                    setFollowButton(!result.followed, button.id);
                }
            }
        });
    });
}

function buttonFollowClick(i) {
    jQuery.ajax({
        async: false,
        type: "POST",
        url: "/ajax/person/swap_follows/",
        data: "person_id=" + $('#followButton'+i).val(),
        success: function(response) {
            result = JSON.parse(response);
            if (! result) {
                console.log('error');
            }
        }
    });
    if ($('#followButton'+i).attr('class') == 'already-follow-person') {
        setFollowButton(true, 'followButton'+i);
    } else {
        setFollowButton(false, 'followButton'+i);
    }
}

function setFollowButton(is_follow, buttonId) {
    if (! is_follow) {
        $('#'+buttonId).attr('class', 'already-follow-person');
        $('#'+buttonId).html('Non seguire più');
    } else {
        $('#'+buttonId).attr('class', 'follow-person');
        $('#'+buttonId).html('Seguire');
    }
}
