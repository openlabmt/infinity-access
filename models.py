# -*- coding: utf-8 -*-

import os
import uuid
import logging

from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User as DjangoUser
from django_countries.fields import CountryField

import sorl  # for thumbnails

logger = logging.getLogger(__name__)


def generate_image_path(instance, filename):
    """Generate a unique file name
    """
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/content', filename)


class ReferenceEntity(models.Model):
    label = models.CharField(u"Label", max_length=80)

    class Meta:
        abstract = True

    def __str__(self):
        return self.label


class UserCategory(ReferenceEntity):
    class Meta:
        verbose_name = u"Categoria utente"
        verbose_name_plural = u"Categorie utente"


class UserProfileQuerySet(models.QuerySet):
    def search(self, search_text):
        return self.filter(
            Q(user__first_name__icontains=search_text.lower()) |
            Q(user__last_name__icontains=search_text.lower()) |
            Q(city__icontains=search_text.lower()) |
            Q(category__label__icontains=search_text.lower()) |
            Q(information__icontains=search_text.lower())
        )

    def like_content(self, content_id):
        return [
            cl.user_profile
            for cl in ContentLike.objects.filter(content__id=content_id)
        ]


class UserProfile(models.Model):
    def list_contents(self):
        return Content.objects.filter(user_created_by__id=self.id)

    def list_followers(self):
        # Get all ids of users that are following
        # the current UserProfile instance
        user_following_ids = (
            UserFollow.objects
            .filter(user_followed__id=self.id)
            .values_list('user_following', flat=True)
        )
        return UserProfile.objects.filter(id__in=user_following_ids)

    def list_following(self):
        # Get all ids of users that
        # the current UserProfile instance is following
        user_followed_ids = (
            UserFollow.objects
            .filter(user_following__id=self.id)
            .values_list('user_followed', flat=True)
        )
        return UserProfile.objects.filter(id__in=user_followed_ids)

    def list_close_to_me(self):
        return (
            UserProfile.objects
            .filter(
                Q(city__iexact=self.city, country=self.country) |
                Q(place_interest_1__iexact=self.city) |
                Q(place_interest_2__iexact=self.city),
                country=self.country
            )
            .exclude(id=self.id)
        )

    def is_followed_by(self, user_profile_followed_id):
        return (
            UserFollow.objects
            .filter(
                user_following__id=user_profile_followed_id,
                user_followed__id=self.id
            )
            .exists()
        )

    def is_following(self, user_profile_to_follow_id):
        return (
            UserFollow.objects
            .filter(
                user_following__id=self.id,
                user_followed__id=user_profile_to_follow_id
            )
            .exists()
        )

    def swap_following(self, user_profile_to_follow):
        user_follow = (
            UserFollow.objects
            .filter(
                user_following__id=self.id,
                user_followed__id=user_profile_to_follow.id
            )
        )
        if user_follow:
            # Already followed => we don't follow
            user_follow.delete()
        else:
            # Follow him!
            user_follow = UserFollow()
            user_follow.user_following = self
            user_follow.user_followed = user_profile_to_follow
            user_follow.save()

    def get_avatar_path(instance, filename):
        """Generate a file name with the username
        """
        ext = filename.split('.')[-1]
        filename = "%s.%s" % (instance.user.get_username(), ext)
        return os.path.join('uploads/avatar', filename)

    @staticmethod
    def get_by_user(user_id):
        return UserProfile.objects.get(user__id=user_id)

    user = models.OneToOneField(DjangoUser, on_delete=models.CASCADE)
    city = models.CharField(u"Città", max_length=20)
    place_interest_1 = models.CharField(
        u"Altra area di interesse (Città)",
        max_length=20, null=True, blank=True
    )
    place_interest_2 = models.CharField(
        u"Altra area di interesse (Città)", 
        max_length=20, null=True, blank=True
    )
    country = CountryField(verbose_name=u"Stato", default="IT")
    category = models.ForeignKey(UserCategory, verbose_name=u"Categoria utente")
    sex = models.CharField(
        u"Sesso", max_length=1, choices=(('m', u'Maschio'), ('f', u'Femine'))
    )
    age = models.PositiveSmallIntegerField(u"Età", null=True, blank=True)
    web_site = models.URLField(u"Sito web", blank=True, null=True)
    information = models.TextField(u"Altre informazioni", null=True, blank=True)
    avatar = sorl.thumbnail.ImageField(
        upload_to=get_avatar_path, null=True, blank=True
    )
    newsletter_registered = models.BooleanField(
        u"Registrazione newsletter", default=True
    )
    followed_users = models.ManyToManyField(
        'UserProfile', through='UserFollow',
        through_fields=('user_following', 'user_followed')
    )
    mail_post_liked = models.BooleanField(
        u"Ricevere una mail per un post 'mi piacce'", default=True
    )
    mail_followed = models.BooleanField(
        u"Ricevere una mail quando sono seguito", default=True
    )
    mail_comment = models.BooleanField(
        u"Ricevere una mail quando un contenuto è stato commentato",
        default=True
    )
    created_on = models.DateTimeField(
        verbose_name=u"Created on", auto_now_add=True
    )
    modified_on = models.DateTimeField(
        verbose_name=u"Updated on", auto_now=True, null=True, blank=True
    )

    objects = UserProfileQuerySet.as_manager()

    class Meta:
        verbose_name = u"Utente IA"
        verbose_name_plural = u"Utenti IA"

    def last_modified(self):
        if self.modified_on is None:
            return self.created_on
        return self.modified_on

    def get_absolute_url(self):
        return reverse('website:account_update', kwargs={'pk': self.pk})

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)


class CommonEntity(models.Model):
    created_by = models.ForeignKey(
        DjangoUser, related_name="%(app_label)s_%(class)s_add"
    )
    created_on = models.DateTimeField(
        verbose_name=u"Created on", auto_now_add=True
    )
    modified_by = models.ForeignKey(
        DjangoUser, related_name="%(app_label)s_%(class)s_update",
        null=True, blank=True
    )
    modified_on = models.DateTimeField(
        verbose_name=u"Updated on", auto_now=True, null=True, blank=True
    )

    class Meta:
        abstract = True


class UserFollow(models.Model):
    followed_on = models.DateTimeField(
        verbose_name=u"Segui dal", auto_now_add=True
    )
    user_following = models.ForeignKey(
        UserProfile, verbose_name=u"Seguito da",
        related_name="%(app_label)s_%(class)s_following",
    )
    user_followed = models.ForeignKey(
        UserProfile, verbose_name=u"Segue",
        related_name="%(app_label)s_%(class)s_followed",
    )

    class Meta:
        unique_together = ('user_following', 'user_followed')

    def __str__(self):
        return "following: %s / followed:%s" % (
            self.user_following, self.user_followed
        )


class ContentCategory(ReferenceEntity):
    # Ciecita, ipovisione, Sordita...

    icon = models.ImageField(upload_to='content_category/icons')

    class Meta:
        verbose_name = u"Categoria di contenuto"
        verbose_name_plural = u"Categorie di contenuto"
        ordering = ['label']


class ContentBaseQuerySet(models.QuerySet):
    def search(self, search_text):
        return self.filter(
            Q(title__icontains=search_text.lower()) |
            Q(text__icontains=search_text.lower())
        )


class ContentBase(models.Model):
    title = models.CharField(u"Titolo", max_length=120)
    text = models.TextField(u"Descrizione")

    user_created_by = models.ForeignKey(
        UserProfile, related_name="%(app_label)s_%(class)s_add"
    )
    user_created_on = models.DateTimeField(
        verbose_name=u"Created on", auto_now_add=True
    )
    user_modified_by = models.ForeignKey(
        UserProfile, related_name="%(app_label)s_%(class)s_update",
        null=True, blank=True
    )
    user_modified_on = models.DateTimeField(
        verbose_name=u"Updated on", auto_now=True, null=True, blank=True
    )

    class Meta:
        abstract = True

    def last_modified(self):
        if self.user_modified_on is None:
            return self.user_created_on
        return self.user_modified_on
    
    def full_title(self):
        return self.title

    def __str__(self):
        return self.title


class ContentQuerySet(ContentBaseQuerySet):
    def list_posts(self):
        return self.filter(content_type='post')

    def list_events(self):
        return self.filter(content_type='event')

    def list_tutorials(self):
        return self.filter(content_type='tutorial')

    def list_by_category(self, cc_id):
        return self.filter(category=cc_id)

    def list_no_category(self):
        return self.filter(category__isnull=True)

    def list_liked_by(self, user_profile_id):
        # Get all ids of users that are following
        # the current UserProfile instance
        contents_ids = (
            ContentLike.objects
            .filter(user_profile__id=user_profile_id)
            .values_list('content', flat=True)
        )
        return self.filter(id__in=contents_ids)


class Content(ContentBase):
    TYPE_POST = 'post'
    TYPE_EVENT = 'event'
    TYPE_TUTORIAL = 'tutorial'
    TYPE_TUTORIAL_PART = 'tutorial_part'
    TYPES_LABELS = {
        # TODO: would be better to access to Post._meta.verbose_name but it's not possible from here
        TYPE_POST: u'Post',
        TYPE_EVENT: u'Evento',
        TYPE_TUTORIAL: u'Tutorial',
        TYPE_TUTORIAL_PART: u'Parte di un tutorial',
    }
    
    def _get_type(self):
        raise NotImplementedError

    def is_post(self):
        try:
            self.post
            return True
        except self.DoesNotExist:
            return False

    def is_event(self):
        try:
            self.event
            return True
        except self.DoesNotExist:
            return False

    def is_tutorial(self):
        try:
            self.tutorial
            return True
        except self.DoesNotExist:
            return False

    def swap_like(self, user_profile):
        cl = ContentLike.objects.filter(
            content__id=self.id, user_profile__id=user_profile.id
        )
        if cl.count() == 0:
            # The user doesn't already likes it
            cl = ContentLike()
            cl.user_profile = user_profile
            cl.content = self
            cl.save()
        else:
            # He liked it but not anymore...
            cl[0].delete()

    def add_comment(self, user_profile, comment):
        cc = ContentComment()
        cc.user_created_by = user_profile
        cc.content = self
        cc.comment = comment
        cc.save()

    def first_media(self):
        return self.contentmedia_set.first()

    def list_other_media(self):
        return self.contentmedia_set.all()[1:]

    category = models.ForeignKey(
        ContentCategory, verbose_name=u"Categoria"
    )
    content_type = models.CharField(
        u"Tipo di contenuto (automatico)", max_length=20, editable=False
    )
    content_type_name = models.CharField(
        u"Nome del tipo di contenuto (automatico)", max_length=20, editable=False
    )

    liked_by_users = models.ManyToManyField(
        UserProfile,
        through='ContentLike',
        through_fields=('content', 'user_profile'),
    )

    objects = ContentQuerySet.as_manager()

    class Meta:
        verbose_name = u"Contenuto"
        verbose_name_plural = u"Contenuti"

    def save(self, *args, **kwargs):
        if not self.pk:
            self.content_type = self._get_type()
            # TODO: see if this field is useful
            self.content_type_name = self.TYPES_LABELS[self.content_type]
        super(Content, self).save(*args, **kwargs)


class Post(Content):
    class Meta:
        verbose_name = u"Post"
        verbose_name_plural = u"Post"

    def _get_type(self):
        return Content.TYPE_POST


class Tutorial(Content):
    # Link to the main part
    main_part = models.ForeignKey(
        u'Tutorial', verbose_name=u"Parte principale", 
        null=True, blank=True, on_delete=models.CASCADE
    )

    num_part = models.PositiveSmallIntegerField(
        verbose_name=u"Numero", default=1
    )

    class Meta:
        verbose_name = u"Tutorial"
        verbose_name_plural = u"Tutorial"

    def _get_type(self):
        if self.main_part is None:
            return Content.TYPE_TUTORIAL
        else:
            return Content.TYPE_TUTORIAL_PART

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.main_part is not None:
                # This is a sub part of the tutorial
                nb_existing = Tutorial.objects.filter(main_part__id=self.main_part.id).count()
                self.num_part = nb_existing + 2
                # Automatically sets the category taking the one of the main part
                self.category = self.main_part.category
            else:
                self.num_part = 1
        super(Tutorial, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.main_part is None:
            # This is the main part of a tutorial
            for tutorial in Tutorial.objects.filter(main_part__id=self.pk):
                tutorial.delete()

        else:
            for tutorial in Tutorial.objects.filter(
                    main_part__id=self.main_part.id, 
                    num_part__gt=self.num_part
                ):
                tutorial.num_part = tutorial.num_part - 1
                tutorial.save()
        super(Tutorial, self).delete(*args, **kwargs)

    def list_sub_parts(self):
        return Tutorial.objects.filter(main_part__id=self.pk)

    def num_parts(self):
        if self.main_part is None:
            return self.list_sub_parts().count() + 1
        return Tutorial.objects.filter(
                main_part__id=self.main_part.id
            ).count() + 1  # +1 because main_part is excluded from this query

    def previous_part(self):
        if self.num_part == 1:
            return None
        if self.num_part == 2:
            return Tutorial.objects.get(id=self.main_part.id)
        return Tutorial.objects.get(
            main_part__id=self.main_part.id, num_part=self.num_part - 1
        )

    def next_part(self):
        if self.num_parts() == 1:
            return None
        if self.num_part == 1:
            return Tutorial.objects.get(main_part__id=self.id, num_part=2)
        if self.num_part == Tutorial.objects.filter(
                main_part__id=self.main_part.id
            ).count() + 1:  # +1 because main_part is excluded from this query
            return None
        return Tutorial.objects.get(
            main_part__id=self.main_part.id, num_part=self.num_part + 1
        )

    def is_last_part(self):
        if self.main_part is None:
            return self.list_sub_parts().count() == 0
        num_parts = Tutorial.objects.filter(main_part__id=self.main_part.id).count() + 1
        return self.num_part == num_parts

    def is_main_part(self):
        return self.main_part is None

    def is_sub_part(self):
        return self.main_part is not None

    def full_title(self):
        if self.main_part is not None:
            return "%s - %s (%d / %d)" % (self.main_part.title, self.title, self.num_part, self.num_parts())
        return self.title


class Event(Content):
    event_on_start = models.DateTimeField(verbose_name=u"Data e orario inizio")
    event_on_end = models.DateTimeField(verbose_name=u"Data e orario fine")
    addr1 = models.CharField(u"Indirizzo 1", max_length=80)
    addr2 = models.CharField(u"Indirizzo 2", max_length=80, null=True, blank=True)
    city = models.CharField(u"Città", max_length=20)
    country = CountryField(verbose_name=u"Stato", default="IT")

    class Meta:
        verbose_name = u"Evento"
        verbose_name_plural = u"Eventi"

    def _get_type(self):
        return Content.TYPE_EVENT

    def is_on_same_day(self):
        if self.event_on_start.year == self.event_on_end.year\
            and self.event_on_start.month == self.event_on_end.month\
            and self.event_on_start.day == self.event_on_end.day:
                return True 
        return False

    def __str__(self):
        return "%s : %s / %s - %s" % (
            self.title, self.event_on_start, self.event_on_end, 
            self.city
        )


class ContentLike(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    user_profile = models.ForeignKey(UserProfile)
    liked_on = models.DateTimeField(
        verbose_name=u"Mi piacce il", auto_now_add=True
    )

    @staticmethod
    def is_liked_by_user(content_id, user_id):
        return (ContentLike.objects
            .filter(content__id=content_id)
            .filter(user_profile__user__id=user_id)
            .count() == 1
        )

    class Meta:
        unique_together = ('content', 'user_profile')


class ContentMedia(models.Model):
    VIDEO = 'v'
    IMAGE = 'i'
    def is_audio(self):
        return self.media_type == self.AUDIO

    def is_image(self):
        return self.media_type == self.IMAGE

    def is_video(self):
        return self.media_type == self.VIDEO

    title = models.CharField(u"Titolo", max_length=80, blank=True, null=True)
    media_type = models.CharField(u"Tipo di media", max_length=1, editable=False)

    url = models.URLField(u"Link del video", blank=True, null=True)
    # file will be uploaded to MEDIA_ROOT/uploads
    image = sorl.thumbnail.ImageField(
        u"Immagine", upload_to=generate_image_path, null=True, blank=True
    )
    content = models.ForeignKey(
        Content, verbose_name=u"Contenuto", on_delete=models.CASCADE
    )

    def save(self, *args, **kwargs):
        if self.image is not None and self.image.name is not None:
            self.media_type = ContentMedia.IMAGE
        elif self.url is not None:
            self.media_type = ContentMedia.VIDEO
        super(ContentMedia, self).save(*args, **kwargs)

    def __str__(self):
        return "%s" % (self.title)

    class Meta:
        verbose_name = u"Media di un contenuto"
        verbose_name_plural = u"Media di un contenuto"


class ContentComment(models.Model):
    title = models.CharField(u"Titolo", max_length=120, blank=True, null=True)
    comment = models.TextField(u"Commento")
    user_created_by = models.ForeignKey(
        UserProfile, related_name="%(app_label)s_%(class)s_add"
    )
    user_created_on = models.DateTimeField(
        verbose_name=u"Creato il", auto_now_add=True
    )
    user_modified_on = models.DateTimeField(
        verbose_name=u"Modificato il", auto_now=True, null=True, blank=True
    )

    content = models.ForeignKey(
        Content, verbose_name=u"Contenuto", on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = u"Commento"
        verbose_name_plural = u"Commenti"
