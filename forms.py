#-*- encoding:utf-8 *-*

from django import forms
from django.forms.formsets import BaseFormSet
from django.utils import timezone
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.password_validation import validate_password

from website.models import *

#_l_default_exclude_fields = ['created_by', 'created_on', 'modified_by', 'modified_on']

_exclude_fields_content_step1 = [
    'category', 'content_type', 'content_type_name', 
    'created_on', 'modified_on', 'user', 'liked_by_users',
    'user_created_by', 'user_modified_by',
]

class CommonForm(forms.ModelForm):
    required_css_class = 'required-field' # Used in the template


class DjangoUserCreateForm(UserCreationForm):
    required_css_class = 'required-field'

    def __init__(self, *args, **kwargs):
        super(DjangoUserCreateForm, self).__init__(*args, **kwargs)
        # Making fields required
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
        self.fields['password1'].required = True
        self.fields['password2'].required = True

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2']


class DjangoUserUpdateForm(UserChangeForm):
    required_css_class = 'required-field'

    def __init__(self, *args, **kwargs):
        super(DjangoUserUpdateForm, self).__init__(*args, **kwargs)
        # Making fields required
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']


class UserProfileFormStep1(CommonForm):
    class Meta:
        model = UserProfile
        exclude = ['avatar', 'category', 'created_on', 'modified_on', 'followed_users', 'user']


class UserProfileFormStep2(CommonForm):
    terms_and_conditions = forms.BooleanField(label=(u"Ho letto ed accetto le condizioni di utilizzo e la privacy"), required=True)
    class Meta:
        model = UserProfile
        fields = ['avatar', 'category', 'terms_and_conditions']


class EventFormStep1(CommonForm):
    def clean_event_on_start(self):
        """ 
        This method is automatically called when form.is_valid()
        is executed in the view
        """
        event_on_start = self.cleaned_data.get('event_on_start')
        if self.instance.id is None:
            if event_on_start is not None:
                if event_on_start < timezone.now():
                    raise forms.ValidationError(
                        u'La data di inizio non può essere anteriore a quella di oggi',
                        code='invalid',
                    )
        return event_on_start

    def clean_event_on_end(self):
        """ 
        This method is automatically called when form.is_valid()
        is executed in the view
        """
        event_on_start = self.cleaned_data.get('event_on_start')
        event_on_end = self.cleaned_data.get('event_on_end')
        if event_on_start is not None and event_on_end is not None:
            if event_on_end < event_on_start:
                raise forms.ValidationError(
                    u'La data di fine deve essere posteriore a quella di inizio',
                    code='invalid',
                )
        return event_on_end

    class Meta:
        model = Event
        exclude = [
            'category', 'content_type', 'content_type_name', 
            'created_on', 'modified_on', 'user', 'liked_by_users',
            'user_created_by', 'user_modified_by',
        ]


class EventFormStep2(CommonForm):
    class Meta:
        model = Event
        fields = ['category',]


class PostFormStep1(CommonForm):
    class Meta:
        model = Post
        exclude = [
            'category', 'content_type', 'content_type_name', 
            'created_on', 'modified_on', 'user', 'liked_by_users',
            'user_created_by', 'user_modified_by',
        ]


class PostFormStep2(CommonForm):
    class Meta:
        model = Post
        fields = ['category',]


class TutorialFormStep1(CommonForm):
    class Meta:
        model = Tutorial
        exclude = _exclude_fields_content_step1 + ['num_part',]


class TutorialFormStep2(CommonForm):
    class Meta:
        model = Tutorial
        fields = ['category',]


class TutorialPartFormStep1(CommonForm):
    class Meta:
        model = Tutorial
        exclude = _exclude_fields_content_step1 + ['num_part', 'main_part']


class TutorialPartFormStep2(CommonForm):
    class Meta:
        model = Tutorial
        fields = []


class MediaLinksForm(CommonForm):
    class Meta:
        model = ContentMedia
        fields = ['url',]


class MediaLinksFormSet(BaseFormSet):
    def clean(self):
        """
        Adds validation to check that no two links have the same URL
        """
        if any(self.errors):
            # TODO: fix me! we enter here if in the model the clean
            # method raise an exception => it should be propagated!
            return
        urls = []
        duplicates = False

        for form in self.forms:
            if form.cleaned_data:
                url = form.cleaned_data['url']

                # Check that no two links have the same URL
                if url:
                    if url in urls:
                        duplicates = True
                    urls.append(url)

                if duplicates:
                    raise forms.ValidationError(
                        u'I link non devono essere gli stessi.',
                        code='duplicate_urls'
                    )

class MediaImageForm(CommonForm):
    class Meta:
        model = ContentMedia
        fields = ['image',]


class MediaImageFormSet(BaseFormSet):
    def clean(self):
        """
        Adds validation
        """
        if any(self.errors):
            # TODO: fix me! we enter here if in the model the clean
            # method raise an exception => it should be propagated!
            return
        images = []
        duplicates = False

        for form in self.forms:
            if form.cleaned_data:
                image = form.cleaned_data['image']

                # Check that no two links have the same image
                if image:
                    if image in images:
                        duplicates = True
                    images.append(image)

                if duplicates:
                    raise forms.ValidationError(
                        u'Le immagini non devono essere le stesse.',
                        code='duplicate_images'
                    )

class PasswordResetRequestForm(forms.Form):
    email = forms.CharField(label=(u"Mail"), max_length=80)


class ChangePasswordForm(forms.ModelForm):
    """
    A form that lets a user change set their password without entering 
    the old password
    """
    error_messages = {
        'password_mismatch': (u"Le due password non corrispondono."),
    }
    password = forms.CharField(label=(u"Nuova password"),
                                    widget=forms.PasswordInput)
    new_password2 = forms.CharField(label=(u"Conferma la nuova password"),
                                    widget=forms.PasswordInput)

    def clean_new_password2(self):
        """ 
        This method is automatically called when form.is_valid()
        is executed in the view
        """
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('new_password2')
        if password and password2:
            if password != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
            validate_password(self.cleaned_data.get('new_password2'), self.instance)
        return password2

    class Meta:
        model = User
        fields = ['password']
