# -*- coding: utf-8 -*-
from ia._version import __version__

def app_version(request):
    return {'VERSION': __version__}
