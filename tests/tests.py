# -*- coding: utf-8 -*-

"""
Tests of the software
"""

from datetime import date, datetime, timedelta

from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.db import transaction
from django.test import TestCase
from django.contrib.auth.models import User
from django.utils import timezone

from website.models import *


def create_user_profile(first_name='lucie', last_name='fer', 
        sex='m', city='Matera', country='IT', category=None,
        **kwargs
    ):
    if kwargs.get('email') is None:
        email_acc = "%s%s" % (first_name.lower()[0], last_name.lower())
        email = "%s@%s.com" % (email_acc, email_acc)
    else:
        email = kwargs['email']

    if kwargs.get('username') is None:
        username = email
    else:
        username = kwargs['username']
    if kwargs.get('password') is None:
        password = 'myPassword'
    else:
        password = kwargs['password']
    user = User.objects.create_user(
        first_name=first_name, 
        last_name=last_name, 
        username=username, 
        email=email,
        password=password,
    )

    user_profile = UserProfile()
    user_profile.user = user
    user_profile.sex = sex
    user_profile.city = city
    user_profile.country = country
    if not category:
        category = UserCategory(label='Foo')
        category.save()
    user_profile.category = category
    # Optional arguments
    if kwargs.get('place_interest_1') is not None:
        user_profile.place_interest_1 = kwargs.get('place_interest_1')
    if kwargs.get('place_interest_2') is not None:
        user_profile.place_interest_2 = kwargs.get('place_interest_2')
    user_profile.save()
    return user_profile

class GenericTest(TestCase):
    def setUp(self):
        self.up_auth = create_user_profile()


class DjangoUserTest(TestCase):
    def test_unique_fields(self):
        u1 = User.objects.create_user(
            first_name='Lucie', 
            last_name='Fer', 
            username='lfer', 
            email='lfer@lfer.com'
        )
        u1.save()

        # Block necessary to unlock transaction of u2.save
        with transaction.atomic():
            u2 = User(first_name='John', last_name='Doe', username='lfer')
            # username already exists => should fail
            self.assertRaises(IntegrityError, u2.save)

        # TODO enable this once we make the email unique
        # See https://gitlab.com/openlabmt/infinity-access/issues/60
        """
        with transaction.atomic():
            u2 = User(first_name='John', last_name='Doe', username='jdoe', email='lfer@lfer.com')
            u2.save()
        """

class UserProfileTest(GenericTest):
    def setUp(self):
        super(UserProfileTest, self).setUp()

    def test_create(self):
        self.assertIsNotNone(self.up_auth) # Created automatically by GenericTest

    def test_close_to_you(self):
        for u in UserProfile.objects.all():
            u.delete()

        user_me = create_user_profile(
            first_name='John', last_name='doe', 
            city='Matera', country='IT'
        )
        user_p2 = create_user_profile(
            first_name='Mickey', last_name='Mouse', 
            city='matera', country='IT'
        )
        user_p3 = create_user_profile(
            first_name='Adam', last_name='Smith', 
            city='Matera', country='US'
        )
        user_p4 = create_user_profile(
            first_name='Trifone', last_name='Girasole', 
            city='Matelica', country='IT'
        )
        user_p5 = create_user_profile(
            first_name='Rocky', last_name='Balboa', 
            city='Milano', place_interest_1='matera', country='IT'
        )
        user_p6 = create_user_profile(
            first_name='Archibald', last_name='Haddock',
            city='Milano', place_interest_1='Torino',
            place_interest_2='Matera', country='IT'
        )

        self.assertEqual(user_me.list_close_to_me().count(), 3)
        should_be_close_to_me = [user_p2, user_p5, user_p6]
        are_close_to_me = True
        for u in should_be_close_to_me:
            if u not in user_me.list_close_to_me():
                self.fail(msg="%s is not in %s" % (u, user_me.list_close_to_me()))
        

class UserProfileFollowTest(GenericTest):
    def setUp(self):
        super(UserProfileFollowTest, self).setUp()

    def test_swap_following(self):
        user_followed = create_user_profile(
            first_name='John', last_name='doe'
        )

        self.up_auth.swap_following(user_followed)
        self.assertTrue(self.up_auth.is_following(user_followed.id))
        self.assertTrue(user_followed.is_followed_by(self.up_auth.id))
        self.assertTrue(user_followed.list_followers().first() == self.up_auth)
        self.assertEqual(self.up_auth.list_followers().count(), 0)
        self.assertTrue(self.up_auth.list_following().first() == user_followed)
        self.assertEqual(user_followed.list_following().count(), 0)

        self.up_auth.swap_following(user_followed)
        self.assertFalse(self.up_auth.is_following(user_followed.id))
        self.assertFalse(user_followed.is_followed_by(self.up_auth.id))


class ContentTest(GenericTest):
    def setUp(self):
        super(ContentTest, self).setUp()

    def _create_post(self, title='Post', text='Text', category='Cat'):
        post = Post(title=title, text=text)
        post.user_created_by = self.up_auth
        category = ContentCategory(label=category)
        category.save()
        post.category = category
        post.save()
        return post

    def test_event_dates(self):
        category = ContentCategory(label='cat')
        category.save()
        event = Event(title=u'Event', text='Text')
        event.user_created_by = self.up_auth
        event.category = category
        event.event_on_start = timezone.now()
        event.event_on_end = timezone.now()
        event.addr1 = 'Via Piave 17'
        event.city = 'Matera'
        event.save()
        self.assertTrue(event.is_on_same_day())

        event.event_on_start = timezone.now()
        event.event_on_end = timezone.now() + timedelta(days=1)
        event.save()
        self.assertFalse(event.is_on_same_day())

    def test_filtering(self):
        categories = []
        for i in range(5):
            post = Post(title=u'Post%s' % i, text='Text%s' % i)
            post.user_created_by = self.up_auth
            category = ContentCategory(label='c%s' % i)
            category.save()
            categories.append(category)
            post.category = category
            post.save()
            self.assertTrue(post.content_type, Content.TYPE_POST)
        self.assertEqual(Post.objects.count(), 5)
        self.assertEqual(ContentCategory.objects.count(), 5)
        
        for i in range(5):
            tutorial = Tutorial(title=u'Tutorial%s' % i, text='Text%s' % i)
            tutorial.user_created_by = self.up_auth
            tutorial.category = categories[i]
            tutorial.save()
            self.assertTrue(tutorial.content_type, Content.TYPE_TUTORIAL)
        self.assertEqual(Tutorial.objects.count(), 5)

        for i in range(5):
            event = Event(title=u'Event%s' % i, text='Text%s' % i)
            event.user_created_by = self.up_auth
            event.category = categories[i]
            event.event_on_start = timezone.now()
            event.event_on_end = timezone.now()
            event.addr1 = 'Via Piave 17'
            event.city = 'Matera'
            event.save()
            self.assertTrue(event.content_type, Content.TYPE_EVENT)
        self.assertEqual(Event.objects.count(), 5)

        self.assertEqual(Content.objects.count(), 15)
        
        self.assertEqual(Content.objects.list_posts().count(), 5)
        qs = Content.objects.filter(title='Post1')
        self.assertEqual(qs.list_posts().count(), 1)
        
        self.assertEqual(Content.objects.list_tutorials().count(), 5)
        qs = Content.objects.filter(title='Tutorial1')
        self.assertEqual(qs.list_tutorials().count(), 1)

        self.assertEqual(Content.objects.list_events().count(), 5)
        qs = Content.objects.filter(title='Event1')
        self.assertEqual(qs.list_events().count(), 1)

        category = ContentCategory.objects.first()
        # Each type of content has the first category
        self.assertEqual(Content.objects.list_by_category(category.id).count(), 3)
        qs = Content.objects.list_events()
        self.assertEqual(qs.list_by_category(category.id).count(), 1)

        self.assertEqual(Content.objects.list_no_category().count(), 0)
        p1 = Post.objects.first()
        p1.category = None
        p1.save()
        self.assertEqual(Content.objects.list_no_category().count(), 1)

    def test_content_media(self):
        post = self._create_post()
        for i in range(5):
            cm = ContentMedia(
                url="http://www.url%d.com" % i
            )
            cm.content = post
            cm.save()
            self.assertEqual(cm.media_type, ContentMedia.VIDEO)
        self.assertEqual(post.first_media().url, "http://www.url0.com")
        other_media_lst = post.list_other_media()
        self.assertEqual(len(other_media_lst), 4)
        for other_media in other_media_lst:
            self.assertNotEqual(other_media.url, "http://www.url0.com")

        cm = ContentMedia(
            image="test", content=post
        )
        cm.save()
        self.assertEqual(cm.media_type, ContentMedia.IMAGE)

class TutorialTest(GenericTest):
    def setUp(self):
        super(TutorialTest, self).setUp()

    def _create_tutorials(self, nb, suffix=''):
        tutorials = []
        # Create some tutorials
        for i in range(nb):
            tutorial = Tutorial(title=u'Tutorial_p%s%s' %(i, suffix), text='Text_p%s%s' %(i, suffix))
            tutorial.user_created_by = self.up_auth
            if i == 0:  # Only for the main part
                category = ContentCategory(label='my_category')
                category.save()
                tutorial.category = category
            else:
                tutorial.main_part = tutorials[0]
            tutorial.save()
            tutorials.append(tutorial)
        return tutorials

    def test_create_tutorial(self):
        tutorials = self._create_tutorials(5)

        for i in range(5):
            self.assertEqual(tutorials[i].num_part, i+1)
            self.assertEqual(tutorials[i].category, tutorials[0].category)
        
        self.assertEqual(tutorials[0].list_sub_parts().count(), 4)
        for i in range(1, 5):
            self.assertEqual(tutorials[i].list_sub_parts().count(), 0)

    def test_delete_tutorial(self):
        tutorials1 = self._create_tutorials(5)
        tutorials2 = self._create_tutorials(7, suffix='other')
        tutorials2_num_parts = [(t.id, t.num_part) for t in tutorials2]

        # When deleting tutorials in the middle, make sure num_part is
        # shifted

        self.assertEqual(tutorials1[3].num_part, 4)
        self.assertEqual(tutorials1[4].num_part, 5)

        tutorial_i3_id = tutorials1[3].id
        tutorial_i4_id = tutorials1[4].id

        tutorials1[2].delete()

        self.assertEqual(Tutorial.objects.get(id=tutorial_i3_id).num_part, 3)
        self.assertEqual(Tutorial.objects.get(id=tutorial_i4_id).num_part, 4)

        # Make sure the other tutorials remain unchanged!
        for id, num_part in tutorials2_num_parts:
            t = Tutorial.objects.get(pk=id)
            self.assertEqual(t.num_part, num_part)

        # When deleting the main tutorial, make sure all others are also
        # deleted

        tutorials1[0].delete()
        # The others remain!
        self.assertEqual(Tutorial.objects.count(), len(tutorials2))

    def test_get_tutorial(self):
        tutorials1 = self._create_tutorials(5)

        self.assertIsNone(tutorials1[0].previous_part())
        self.assertTrue(tutorials1[0].is_main_part())
        self.assertFalse(tutorials1[0].is_sub_part())
        for i in range(1, len(tutorials1)):
            self.assertTrue(tutorials1[i].is_sub_part())
            self.assertEqual(tutorials1[i].previous_part().id, tutorials1[i-1].id)

        for i in range(len(tutorials1)-1):
            self.assertEqual(tutorials1[i].next_part().id, tutorials1[i+1].id)
        self.assertIsNone(tutorials1[len(tutorials1)-1].next_part())

        tutorials3 = self._create_tutorials(1)
        self.assertIsNone(tutorials3[0].previous_part())
        self.assertIsNone(tutorials3[0].next_part())
        self.assertTrue(tutorials3[0].is_main_part())
        self.assertFalse(tutorials3[0].is_sub_part())
        self.assertTrue(tutorials3[0].is_last_part())
        self.assertEqual(tutorials3[0].num_parts(), 1)

        tutorials4 = self._create_tutorials(3)
        for i in range(len(tutorials4)):
            self.assertEqual(tutorials4[i].num_parts(), 3)
        for i in range(len(tutorials4)-1):
            self.assertFalse(tutorials4[i].is_last_part())
        self.assertTrue(tutorials4[len(tutorials4)-1].is_last_part())

class ContentLikeTest(GenericTest):
    def setUp(self):
        super(ContentLikeTest, self).setUp()

    def test_contents_liked_by_user(self):
        user_p1 = create_user_profile(
            first_name='John', last_name='doe', 
            username='jdoe', email='jdoe@jdoe.com'
        )

        post1 = Post(title=u'Post1', text='Text1')
        post1.user_created_by = self.up_auth
        post1.save()
        post1.swap_like(user_p1)

        post2 = Post(title=u'Post2', text='Text2')
        post2.user_created_by = self.up_auth
        post2.save()
        post2.swap_like(user_p1)

        num_like = 0
        for content in Content.objects.list_liked_by(user_p1.id):
            num_like += 1
            self.assertTrue(isinstance(content, Content))
        self.assertEqual(num_like, 2)
        self.assertEqual(Content.objects.list_liked_by(self.up_auth.id).count(), 0)

        # Use a pre-filter
        self.assertEqual(Content.objects.list_liked_by(user_p1.id).count(), 2)
        qsPost1 = Content.objects.filter(title=u'Post1')
        self.assertEqual(qsPost1.count(), 1)
        self.assertEqual(qsPost1.list_liked_by(user_p1.id).count(), 1)

        # Remove a like on post2
        post2.swap_like(user_p1)
        self.assertEqual(Content.objects.list_liked_by(user_p1.id).count(), 1)
