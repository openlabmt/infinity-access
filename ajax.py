import logging, simplejson

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from website.models import *
from website.views import LOGIN_URL

logger = logging.getLogger(__name__)


@csrf_exempt
@login_required(login_url=LOGIN_URL)
def swap_like_content(request):
    user_profile = UserProfile.objects.get(user__id=request.user.id)
    content_id = request.POST['content_id']

    if user_profile:
        content = Content.objects.get(id=content_id)
        if content == None:
            logger.error("Content id not found: %s" % content_id)
        else:
            content.swap_like(user_profile)
    else:
        logger.warning("No user profile found for user: %s" % user)
    return HttpResponse(simplejson.dumps({}))

@csrf_exempt
def is_content_liked(request):
    liked = False
    content_id = request.POST['content_id']
    user_auth = request.user.is_authenticated()
    if user_auth:
        liked = ContentLike.is_liked_by_user(content_id, request.user.id)
    return HttpResponse(simplejson.dumps({'user_auth': user_auth, 'liked': liked}))

@csrf_exempt
@login_required(login_url=LOGIN_URL)
def swap_follows(request):
    user_profile_con = UserProfile.objects.get(user__id=request.user.id)
    person_id = request.POST['person_id']

    if user_profile_con:
        user_profile_to_follow = UserProfile.objects.get(id=person_id)
        user_profile_con.swap_following(user_profile_to_follow)
    else:
        logger.error("No user profile found for user: %s" % user)
    return HttpResponse(simplejson.dumps({}))

@csrf_exempt
def is_user_followed(request):
    followed = False
    person_id = request.POST['person_id']
    user_auth = request.user.is_authenticated()
    if user_auth:
        user_profile_con = UserProfile.objects.get(user__id=request.user.id)
        followed = user_profile_con.is_following(person_id)
    return HttpResponse(simplejson.dumps({'user_auth': user_auth, 'followed': followed}))
