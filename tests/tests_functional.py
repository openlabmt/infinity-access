import datetime
import re

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from website.models import *
from website.tests.tests import create_user_profile


class SeleniumTestCase(StaticLiveServerTestCase):
    fixtures = ['ref_data_test.json']
    user_login = 'test@test.org'
    user_password = 'myReallyStrongPassword'
    default_user_created = False

    @classmethod
    def setUpClass(cls):
        super(SeleniumTestCase, cls).setUpClass()
        cls.selenium = WebDriver()
        cls.selenium.maximize_window();

    @classmethod
    def tearDownClass(cls):
        #pass
        cls.selenium.quit()
        super(SeleniumTestCase, cls).tearDownClass()

    def setUp(self):
        if not SeleniumTestCase.default_user_created:
            self._go_home()
            self._create_user('first_name', 'last_name', self.user_login, self.user_password)
            SeleniumTestCase.default_user_created = True
        
    def exists_by_id(self, element_id):
        try:
            self.selenium.find_element_by_id(element_id)
        except NoSuchElementException:
            return False
        return True

    def page_contains_text(self, text):
        text_found = re.search(text, self.selenium.page_source)
        return text_found is not None

    def _go_home(self):
        self.selenium.get(self.live_server_url)
        self._check_is_home_page()

    def _login(self, username, password):
        self.selenium.get('%s%s' % (self.live_server_url,  "/account/login/"))
        self._check_is_login_page()
        username_f = self.selenium.find_element_by_id("username")
        username_f.send_keys(username)
        password_f = self.selenium.find_element_by_id("password")
        password_f.send_keys(password)
        self.selenium.find_element_by_id("loginButton").click()

    def _logout(self):
        self.selenium.find_element_by_id('userProfileMenuLink').click()
        self.selenium.find_element_by_id('logoutLink').click()
        self.assertFalse(self.exists_by_id('userProfileMenuLink'))

    def _default_login(self):
        self._login(self.user_login, self.user_password)

    def _click_add_content_menu(self):
        self.selenium.find_element_by_css_selector("a.dropdown-toggle > img").click()

    def _check_is_home_page(self):
        self.assertTrue(self.exists_by_id("homePage"))

    def _check_is_login_page(self):
        self.assertTrue(self.exists_by_id("loginPage"))

    def _create_user(self, first_name, last_name, email, password, city="Matera"):
        self.selenium.find_element_by_id("registerLink").click()
        self.selenium.find_element_by_id("id_user-first_name").send_keys(first_name)
        self.selenium.find_element_by_id("id_user-last_name").send_keys(last_name)
        self.selenium.find_element_by_id("id_user-email").send_keys(email)
        self.selenium.find_element_by_id("id_user-password1").send_keys(password)
        self.selenium.find_element_by_id("id_user-password2").send_keys(password)
        self.selenium.find_element_by_id("id_user_profile-city").send_keys(city)
        self.selenium.find_element_by_id("id_user_profile-sex").send_keys("m")
        self.selenium.find_element_by_id("submitButton").click()

        self.assertFalse(self.exists_by_id("formErrors"))
        # Check we are on the next page
        self.assertTrue(self.exists_by_id("id_category"))
        category_elements = self.selenium.find_elements_by_xpath("//*[contains(@id, 'category-')]")
        category_elements[0].click()  # select the first category found
        self.assertFalse(self.selenium.find_element_by_id("id_terms_and_conditions").is_selected())
        self.selenium.find_element_by_id("id_terms_and_conditions").click()
        self.selenium.find_element_by_id("submitButton").click()

    def _create_post(self, title='My post', description='My description', 
            id_category=8, 
            video_urls=[
                'https://www.youtube.com/watch?v=IY15J5ItdGI', 
                'https://www.youtube.com/watch?v=19fp3kxhRXQ'
            ]
        ):
        self._click_add_content_menu()
        self.selenium.find_element_by_id("createPost").click()
        self.selenium.find_element_by_id("id_title").clear()
        self.selenium.find_element_by_id("id_title").send_keys(title)
        self.selenium.find_element_by_id("id_text").clear()
        self.selenium.find_element_by_id("id_text").send_keys(description)
        self.selenium.find_element_by_id("submitButton").click()
        self.selenium.find_element_by_id("category-8").click()
        self.selenium.find_element_by_id("id_form-0-url").clear()
        self.selenium.find_element_by_id("id_form-0-url").send_keys(video_urls[0])
        self.selenium.find_element_by_id("id_form-1-url").clear()
        self.selenium.find_element_by_id("id_form-1-url").send_keys(video_urls[1])
        self.selenium.find_element_by_id("submitButton").click()

        # Check we are on the next page (detail of a content)
        self.assertTrue(self.exists_by_id("contentDetailsPage"))
        self.assertTrue(self.page_contains_text('www.youtube.com'))

    def _create_event(self):
        self._click_add_content_menu()
        self.selenium.find_element_by_id("createEvent").click()
        self.selenium.find_element_by_id("id_title").clear()
        self.selenium.find_element_by_id("id_title").send_keys("Event1")
        self.selenium.find_element_by_id("id_text").clear()
        self.selenium.find_element_by_id("id_text").send_keys("Description event 1")

        today = datetime.datetime.now()

        # Error: event can't start before today
        yesterday = datetime.timedelta(days=-1) + today
        yesterday_str = "%s %s" % (yesterday.strftime("%d/%m/%Y"), yesterday.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_start").clear()
        self.selenium.find_element_by_id("id_event_on_start").send_keys(yesterday_str)
        today_str = "%s %s" % (today.strftime("%d/%m/%Y"), today.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_end").clear()
        self.selenium.find_element_by_id("id_event_on_end").send_keys(today_str)
               
        self.selenium.find_element_by_id("id_addr1").clear()
        self.selenium.find_element_by_id("id_addr1").send_keys("Via Piave, 17")
        self.selenium.find_element_by_id("id_city").clear()
        self.selenium.find_element_by_id("id_city").send_keys("Matera")
        self.selenium.find_element_by_id("submitButton").click()
        self.assertTrue(self.exists_by_id("formErrors"))

        # Error: event can't start after it ends
        future = datetime.timedelta(days=10) + today
        future_str = "%s %s" % (future.strftime("%d/%m/%Y"), future.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_start").clear()
        self.selenium.find_element_by_id("id_event_on_start").send_keys(future_str)
        tomorrow = datetime.timedelta(days=1) + today
        tomorrow_str = "%s %s" % (tomorrow.strftime("%d/%m/%Y"), tomorrow.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_end").clear()
        self.selenium.find_element_by_id("id_event_on_end").send_keys(tomorrow_str)
        self.selenium.find_element_by_id("submitButton").click()
        self.assertTrue(self.exists_by_id("formErrors"))

        # Now let's do it right!
        future1 = datetime.timedelta(days=10) + today
        future1_str = "%s %s" % (future1.strftime("%d/%m/%Y"), future1.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_start").clear()
        self.selenium.find_element_by_id("id_event_on_start").send_keys(future1_str)
        future2 = datetime.timedelta(days=10) + today
        future2_str = "%s %s" % (future2.strftime("%d/%m/%Y"), future2.strftime("%H:%M"))
        self.selenium.find_element_by_id("id_event_on_end").clear()
        self.selenium.find_element_by_id("id_event_on_end").send_keys(future2_str)
        self.selenium.find_element_by_id("submitButton").click()
        ### Step 2
        self.selenium.find_element_by_id("category-8").click()
        self.selenium.find_element_by_id("id_form-0-url").clear()

        self.selenium.find_element_by_id("submitButton").click()

        # Check we are on the next page (detail of a content)
        self.assertTrue(self.exists_by_id("contentDetailsPage"))

        #self._go_home()
        #self.selenium.find_element_by_css_selector("h1").click()
        """
        # Let's try to change the event
        self.selenium.find_element_by_id("changeButton").click()
        self.selenium.find_element_by_id("id_title").clear()
        new_title = "Event1 changed"
        self.selenium.find_element_by_id("id_title").send_keys(new_title)        
        self.selenium.find_element_by_id("submitButton").click()
        ### Step 2
        self.selenium.find_element_by_id("submitButton").click()
        # Check we are on the next page (detail of a content)
        self.assertTrue(self.exists_by_id("contentDetailsPage"))
        self.assertTrue(self.page_contains_text(new_title))

        self._logout()
        """

    def test_home(self):
        self._go_home()

    def test_register(self):
        email = 'name1@iatest.org'
        username = email
        password = 'myStrongPassword'
        self._go_home()
        self.selenium.find_element_by_id("registerLink").click()
        self.assertTrue(self.exists_by_id("accountEditPage"))
        self.selenium.find_element_by_id("id_user-first_name").send_keys("FirstName1")
        self.selenium.find_element_by_id("submitButton").click()
        self.assertTrue(self.exists_by_id("formErrors"))

        self.selenium.find_element_by_id("id_user-first_name").clear()
        self._create_user("FirstName1", "LastName1", email, password)
        # Page that confirms the account was created
        self.selenium.find_element_by_id("loginLink").click()

        # Login page
        self._login(username, password)

        # Home page
        self._check_is_home_page()
        self.selenium.find_element_by_id('userProfileMenuLink').click()
        self.selenium.find_element_by_id('updateAccountLink').click()

        # Update account page
        self.assertTrue(self.exists_by_id("accountEditPage"))
        self.selenium.find_element_by_id("id_user_profile-information").send_keys("Hello, I'm Marc!")
        self.selenium.find_element_by_id("submitButton").click()
        self.assertFalse(self.exists_by_id("formErrors"))
        self.assertTrue(self.selenium.find_element_by_id("id_terms_and_conditions").is_selected())
        self.selenium.find_element_by_id("submitButton").click()
        self.assertTrue(self.exists_by_id("accountEditPage"))
        self.assertFalse(self.exists_by_id("formErrors"))
        last_change_text = self.selenium.find_element_by_id("lastUpdatedText").text
        cur_date = datetime.date.today().strftime("%d/%m/%Y")
        cur_time = datetime.datetime.now().strftime("%H:%M")
        last_changed_text = self.selenium.find_element_by_id("lastUpdatedText").text
        self.assertTrue(cur_date in last_changed_text)
        self.assertTrue(cur_time in last_changed_text)

    def test_wrong_login(self):
        self._login("foo", "bar")
        self._check_is_login_page()
        self.assertTrue(self.exists_by_id("msgLst"))

    def test_create_contents(self):
        self._default_login()

        self._create_post(title='Test post 1', description='Description test post 1')
        self.selenium.find_element_by_id("searchField").clear()
        self.selenium.find_element_by_id("searchField").send_keys("post 1")
        self.selenium.find_element_by_id("searchButton").click()
        self.assertTrue(self.exists_by_id("contentItem"))
        self.assertTrue(self.page_contains_text('Test post 1'))

        self._create_event()

        self._logout()

    def test_follow(self):
        create_user_profile(
            first_name='UserWillBeFollowed1-fn',
            last_name='UserWillBeFollowed1-ln',
            email='user1@user1.org',
            password='myStrongPassword1',
            city='Matera',
        )

        create_user_profile(
            first_name='User2-fn',
            last_name='User2-ln',
            email='user2@user2.org',
            password='myStrongPassword2',
            city='Matera',
        )

        self._login('user2@user2.org', 'myStrongPassword2')

        self._create_post(title='Matera', description='The wonderful town of Matera')

        self._go_home()
        self.selenium.find_element_by_id("searchField").clear()
        self.selenium.find_element_by_id("searchField").send_keys("Matera")
        self.selenium.find_element_by_id("searchButton").click()
        self.exists_by_id("searchResultContentsPage")
        self.selenium.find_element_by_id("personTabButton").click()

        body = self.selenium.find_element_by_tag_name('body')
        self.assertIn('UserWillBeFollowed1-fn', body.text)

