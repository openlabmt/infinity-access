# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.views.generic import TemplateView

# It uses LOGIN_URL from settings.py
# from django.contrib.auth.decorators import login_required

from website import ajax, views

urlpatterns = [
    url(r'^$', views.Home.as_view(), name='home'),
    url(
        r'^home_filter/(?P<content_type>\w+)/(?P<cc_id>-?\d+)/$',
        views.Home.as_view(), name='home_filter'
    ),

    url(
        r'^person/(?P<person_id>\d+)/$', views.ContentsByPerson.as_view(),
        name='contents_by_person'
    ),
    url(
        r'^person/(?P<content_type>\w+)/(?P<cc_id>-?\d+)/(?P<person_id>\d+)'
        + '/(?P<my_contents>\w+)/$', views.ContentsByPerson.as_view(),
        name='contents_by_person'
    ),
    url(
        r'^my_profile/(?P<user_id>\d+)/$',
        views.ContentsByPerson.as_view(), name='my_profile'
    ),


    url(
        r'^search_contents/$', views.SearchResultsContents.as_view(),
        name='search_results_contents'
    ),

    url(
        r'^search_persons/$', views.SearchResultsPersons.as_view(),
        name='search_results_persons'
    ),

    url(r'^filter_persons/like_content/(?P<content_id>\d+)/$',
        views.FilterPersons.as_view(), name='filter_persons_like_content'),
    url(r'^filter_persons/by_category/(?P<user_category_id>\d+)/$',
        views.FilterPersons.as_view(), name='filter_persons_by_category'),
    url(r'^filter_persons/(?P<person_id>\d+)/(?P<filter>\w+)/$',
        views.FilterPersons.as_view(), name='filter_persons'),
    url(
        r'^account_add/(?P<step>\d+)/$',
        views.AccountAdd.as_view(), name="account_add"
    ),
    url(
        r'^account_update/(?P<user_id>\d+)/(?P<step>\d+)/$',
        views.AccountUpdate.as_view(), name="account_update"
    ),
    url(
        r'^account_created/$', TemplateView.as_view(
            template_name='website/account/account_created.html'
        ),
        name="account_created"
    ),

    url(
        r'^content_details/(?P<pk>\d+)/(?P<content_type>\w+)/$',
        views.ContentDetails.as_view(), name="content_details"
    ),
    # POST url
    url(
        r'^content_details/add_comment/$',
        views.add_comment_to_content, name="add_comment_to_content"
    ),
    url(
        r'^content_add/(?P<step>\d+)/(?P<content_type>\w+)/$',
        views.ContentAdd.as_view(), name="content_add"
    ),
    url(
        r'^content_update/(?P<content_id>\d+)/(?P<step>\d+)/(?P<content_type>\w+)/$',
        views.ContentUpdate.as_view(), name="content_update"
    ),
    url(
        r'^tutorial_part_display/(?P<cur_id>\d+)/(?P<direction>\w+)/$',
        views.tutorial_part_display, name="tutorial_part_display"
    ),
    url(
        r'^tutorial_part_add/(?P<prev_content_id>\d+)/$',
        views.tutorial_part_add, name="tutorial_part_add"
    ),
    url(
        r'^about_us/$', TemplateView.as_view(
            template_name='website/about_us.html'
        ),
        name="about_us"
    ),
    url(
        r'^terms_and_conditions/$', TemplateView.as_view(
            template_name='website/terms_and_conditions.html'
        ),
        name="terms_and_conditions"
    ),
    url(
        r'^privacy/$', TemplateView.as_view(
            template_name='website/privacy.html'
        ),
        name="privacy"
    ),
    url(
        r'^contacts/$', TemplateView.as_view(
            template_name='website/contacts.html'
        ),
        name="contacts"
    ),

    url(r'^account/login/$', views.Login.as_view(), name='login'),
    url(r'^account/logout/$', views.Logout.as_view(), name='logout'),

    url(
        r'^error/$', TemplateView.as_view(
            template_name='website/generic_error_page.html'
        ),
        name="generic_error_page"
    ),

    # POST URL (AJAX)
    url(
        r'^ajax/content/swap_like_it/$',
        ajax.swap_like_content, name="swap_like_content"
    ),
    url(
        r'^ajax/content/is_liked/$',
        ajax.is_content_liked, name="is_content_liked"
    ),
    # These URLs are directly used in follow_person.js
    url(r'^ajax/person/swap_follows/$', ajax.swap_follows, name="swap_follows"),
    url(
        r'^ajax/person/is_user_followed/$',
        ajax.is_user_followed, name="is_user_followed"
    ),

    url(
        r'^account/change_password/(?P<pk>\d+)/$', 
        views.ChangePassword.as_view(), name="change_password"
    ),

    url(r'^account/reset_password_request/(?P<init>\w*)', views.ResetPasswordRequest.as_view(), name="reset_password_request"),
    url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.ResetPasswordConfirm.as_view(),name='reset_password_confirm'),
    url(
        r'^account/reset_password_link_sent/$', TemplateView.as_view(
            template_name='website/account/reset_password_link_sent.html'
        ),
        name="reset_password_link_sent"
    ),
    url(
        r'^account/reset_password_done/$', TemplateView.as_view(
            template_name='website/account/reset_password_done.html'
        ),
        name="reset_password_done"
    ),

] + static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
)  # Only for development purposes

"""
print("media root: %s" % settings.MEDIA_ROOT)
print("media url: %s" % settings.MEDIA_URL)
print(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
"""
